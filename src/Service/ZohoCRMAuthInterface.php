<?php

namespace Drupal\zoho_crm_integration\Service;

/**
 * The Zoho CRM Auth Interface.
 */
interface ZohoCRMAuthInterface {

  /**
   * Zoho CRM config settings.
   */
  const SETTINGS = 'zoho_crm_integration.settings';

  /**
   * Zoho CRM settings route.
   */
  const ROUTE = 'zoho_crm_integration.settings';

}
