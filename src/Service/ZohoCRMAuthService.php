<?php

namespace Drupal\zoho_crm_integration\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Url;
use Drupal\Core\File\FileSystem;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use zcrmsdk\crm\setup\restclient\ZCRMRestClient;
use zcrmsdk\crm\utility\APIConstants;
use zcrmsdk\oauth\exception\ZohoOAuthException;
use zcrmsdk\oauth\ZohoOAuth;

/**
 * The Zoho CRM Auth Service.
 */
class ZohoCRMAuthService implements ZohoCRMAuthInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal Config service.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Authorization Redirect URL.
   *
   * @var string
   */
  public $redirectUrl;

  /**
   * The list of authorized scopes.
   *
   * @var array
   */
  protected $scope;

  /**
   * Client ID.
   *
   * @var string
   */
  protected $clientId;

  /**
   * The client secret property.
   *
   * @var string
   */
  protected $clientSecret;

  /**
   * User e-mail.
   *
   * @var string
   */
  protected $userEmail;

  /**
   * Zoho domain.
   *
   * @var string
   */
  protected $zohoDomain;

  /**
   * Revoke URL.
   *
   * @var string
   */
  protected $revokeUrl;

  /**
   * Grant URL.
   *
   * @var string
   */
  protected $grantUrl;

  /**
   * File System Service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The generated refresh token.
   *
   * @var string
   */
  protected $refreshToken;

  /**
   * Guzzle HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Drupal URL service.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * Drupal Scopes Services.
   *
   * @var \Drupal\zoho_crm_integration\Service\ZohoCRMIntegrationScopesService
   */
  protected $scopesService;

  /**
   * The custom Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The Zoho api.
   *
   * @var string
   */
  private $zohoApi;

  /**
   * The account array.
   *
   * @var string[]
   */
  private $zohoAccountsArray;

  /**
   * The api array.
   *
   * @var string[]
   */
  private $zohoApiArray;

  /**
   * Constructs a new ZohoCRMAuthService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal Config Factory service.
   * @param \Drupal\Core\File\FileSystem $file_system
   *   Drupal File System service.
   * @param \Drupal\zoho_crm_integration\Service\ZohoCRMIntegrationScopesService $scopes_service
   *   Drupal Scopes services.
   * @param \GuzzleHttp\Client $http_client
   *   Drupal HTTP Client service.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   Drupal URL service.
   * @param \Psr\Log\LoggerInterface $logger
   *   Custom ZohoCRM module Logger.
   */
  public function __construct(ConfigFactoryInterface $config_factory, FileSystem $file_system, ZohoCRMIntegrationScopesService $scopes_service, Client $http_client, UrlGeneratorInterface $url_generator, LoggerInterface $logger) {
    // Getting services.
    $this->scopesService = $scopes_service;
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->refreshToken = NULL;
    $this->urlGenerator = $url_generator;
    $this->logger = $logger;

    // Getting the saved refresh token.
    $refresh_token = $this->configFactory->get(self::SETTINGS)->get('refresh_token');
    if (
      !is_null($refresh_token) &&
      !is_bool($refresh_token)
    ) {
      $this->refreshToken = $refresh_token;
    }

    // The zoho accounts array.
    $this->zohoAccountsArray = [
      'com' => 'https://accounts.zoho.com',
      'eu' => 'https://accounts.zoho.eu',
      'cn' => 'https://accounts.zoho.com.cn',
      'in' => 'https://accounts.zoho.in',
    ];

    // The zoho api array.
    $this->zohoApiArray = [
      'com' => 'www.zohoapis.com',
      'eu' => 'www.zohoapis.eu',
      'cn' => 'www.zohoapis.com.cn',
      'in' => 'www.zohoapis.in',
    ];

    $this->scope = $scopes_service->getScopesParameters();
    $this->clientId = $config_factory->get(self::SETTINGS)->get('client_id');
    $this->clientSecret = $config_factory->get(self::SETTINGS)->get('client_secret');
    $this->userEmail = $config_factory->get(self::SETTINGS)->get('current_user_email');
    $this->zohoDomain = $this->zohoAccountsArray[$config_factory->get(self::SETTINGS)->get('zoho_domain') ?? 'com'];
    $this->zohoApi = $this->zohoApiArray[$config_factory->get(self::SETTINGS)->get('zoho_domain') ?? 'com'];
    $this->fileSystem = $file_system->realPath('private://');

    // Use ZohoOAuth class only if the class exist.
    if ($this->checkSdkClass() && $this->hasClientId()) {
      $this->initialize();
      $this->grantUrl = ZohoOAuth::getGrantURL();
      $this->revokeUrl = ZohoOAuth::getRevokeTokenURL();
    }
  }

  /**
   * Returns authorization redirect URL.
   *
   * @return string
   *   The URL.
   */
  private function getRedirectUrl() {
    if (!$this->redirectUrl) {
      $url = Url::fromRoute(self::ROUTE, [], ['absolute' => TRUE]);
      $url = $url->toString();

      if ($url instanceof GeneratedUrl) {
        $url = $url->getGeneratedUrl();
      }

      $this->redirectUrl = $url;
    }

    return $this->redirectUrl;
  }

  /**
   * Get magic method.
   *
   * @return mixed
   *   Returns variable based on the get.
   */
  public function __get($name) {
    return $this->$name;
  }

  /**
   * Build authorization URL.
   *
   * @return string
   *   Full authorization URL.
   */
  public function getAuthorizationUrl(): string {
    $params = [
      'prompt' => 'consent',
      'scope' => $this->scope,
      'client_id' => $this->clientId,
      'response_type' => 'code',
      'access_type' => 'offline',
      'redirect_uri' => $this->getRedirectUrl(),
    ];
    $query_string = http_build_query($params);

    return "{$this->grantUrl}?{$query_string}";
  }

  /**
   * Generate the Revoke URL.
   *
   * @return bool
   *   Return TRUE if HTTP request works or FALSE.
   *
   * @throws \zcrmsdk\oauth\exception\ZohoOAuthException
   */
  public function revokeRefreshToken(): bool {
    try {
      $this->initialize();
      $persistenceTokens = ZohoOAuth::getPersistenceHandlerInstance()->getOAuthTokens($this->userEmail);
      $refresh_token = $persistenceTokens->getRefreshToken();
      $request = $this->httpClient->request('POST', "{$this->revokeUrl}?token={$refresh_token}");

      if ($request->getStatusCode() == '200') {
        return TRUE;
      }
    }
    catch (GuzzleException $e) {
      $this->logger->alert("Error trying revoke Zoho CRM API Refresh Token. Exception message: {$e->getMessage()}");
      return FALSE;
    }

    return FALSE;
  }

  /**
   * Check if Client ID exists.
   *
   * @return bool
   *   True if Client ID exists, otherwise false.
   */
  public function hasClientId(): bool {
    return $this->clientId !== NULL && $this->clientId !== '';
  }

  /**
   * Get authorization parameters.
   *
   * @return array
   *   Authorization parameters.
   */
  private function getAuthorizationParams(): array {
    return [
      'client_id' => $this->clientId,
      'client_secret' => $this->clientSecret,
      'redirect_uri' => $this->getRedirectUrl(),
      'currentUserEmail' => $this->userEmail,
      'token_persistence_path' => $this->fileSystem,
      'accounts_url' => $this->zohoDomain,
      'apiBaseUrl' => $this->zohoApi,
      APIConstants::APPLICATION_LOGFILE_PATH => $this->fileSystem,
    ];
  }

  /**
   * Generate access token.
   *
   * @param string $grant_token
   *   Grant token.
   *
   * @return bool
   *   Returns true if the access token could be generated.
   */
  public function generateAccessToken(string $grant_token): bool {
    try {
      $this->initialize();
      $oauth_client = ZohoOAuth::getClientInstance();
      $tokens = $oauth_client->generateAccessToken($grant_token);

      if (is_object($tokens)) {
        $this->refreshToken = $tokens->getRefreshToken();
        $this->configFactory->getEditable(self::SETTINGS)->set('refresh_token', $this->refreshToken)->save();

        $response = new RedirectResponse($this->urlGenerator->generateFromRoute('zoho_crm_integration.settings'));
        $response->send();
        return TRUE;
      }
    }
    catch (ZohoOAuthException $e) {
      $this->logger->alert("Error trying generate Zoho CRM API Access Token from Grant Token. Exception message: {$e->getMessage()}");
    }
    return FALSE;
  }

  /**
   * Check if is possible to connect on API creating a Lead.
   *
   * @return bool
   *   Return TRUE if you could success connect on user/info endpoint.
   */
  public function checkConnection(): bool {
    try {
      $this->initialize();
      $oauth_client = ZohoOAuth::getClientInstance();
      $accessToken = $oauth_client->getAccessToken($this->userEmail);
      $user = $oauth_client->getUserEmailIdFromIAM($accessToken);

      return ($user !== NULL);
    }
    catch (ZohoOAuthException $e) {
      $this->logger->alert("Error trying test Zoho CRM API connection. Exception message: {$e->getMessage()}");
      return FALSE;
    }
  }

  /**
   * Initialize ZCRMRestClient.
   */
  public function initialize(): void {
    ZCRMRestClient::initialize($this->getAuthorizationParams());
  }

  /**
   * Check for ZohoOAuth.php file on vendor folder.
   *
   * @return bool
   *   Return TRUE if ZohoOAuth.php file exist.
   */
  public function checkSdkClass(): bool {
    $file = DRUPAL_ROOT . '/../vendor/zohocrm/php-sdk/src/oauth/ZohoOAuth.php';
    $archive_file = DRUPAL_ROOT . '/../vendor/zohocrm/php-sdk-archive/src/oauth/ZohoOAuth.php';

    return (file_exists($file)) || (file_exists($archive_file));
  }

}
